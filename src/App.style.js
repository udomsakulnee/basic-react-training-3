import styled from "styled-components";
import {breakpoints} from "./themes/style";

export const ImageContainer = styled.div`
  flex: 1;

  @media screen and (max-width: ${breakpoints.sm}) {
    display: none;
  }
`;

export const Label = styled.div`
  // display: flex;
  // align-items: center;
  line-height: 45px;
  height: 45px;
`;

export const CheckboxWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;

  @media screen and (max-width: ${breakpoints.sm}) {
    display: block;
    .btn {
      width: 100%;
    }
  }
`;