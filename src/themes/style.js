export const breakpoints = {
  xs: "576px",
  sm: "768px",
  md: "992px",
  lg: "1200px",
  xl: "1400px",
};

export const dimensions = {
  marginBottom: "10px",
};

export const colors = {
  primary: "#0d6efd",
  white: "#ffffff",
};
