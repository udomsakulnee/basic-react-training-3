import React, {useState} from "react";
import styles from "./App.module.css";
import { Row, Col, Button } from "reactstrap";
import { ImageContainer, Label, CheckboxWrapper } from "./App.style";
import { InputWithLabel, Checkbox } from "./components/shared";

const App = () => {
  const [isRemember, setIsRemember] = useState(false);
  console.log(isRemember);

  return (
    <div className={styles.container}>
      <div className={styles.wrapper}>
        <div className={styles.form}>
          <h4 className="text-center mb-4">Title Example Form</h4>

          <Row>
            <Col md={4}>
              <Label>Username :</Label>
            </Col>
            <Col md={8}>
              <InputWithLabel value="test" onChange={() => {}} />
            </Col>
          </Row>
          <Row>
            <Col md={4}>
              <Label>Password :</Label>
            </Col>
            <Col md={8}>
              <InputWithLabel value="test" onChange={() => {}} />
            </Col>
          </Row>

          <hr />

          <CheckboxWrapper>
            <Checkbox value={isRemember} onChange={setIsRemember}/>
            <Button color="primary">Login</Button>
          </CheckboxWrapper>
        </div>
        <ImageContainer style={{ backgroundColor: "green" }}>Y</ImageContainer>
      </div>
    </div>
  );
};

export default App;
