import styled from "styled-components";
import {Input as InputStrap} from "reactstrap";
import {dimensions} from "../../themes/style";

export const Container = styled.div`
    position: relative;
    margin-bottom: ${dimensions.marginBottom};
`;

export const Icon = styled.div`
    width: 50px;
    height: 45px;
    position: absolute;
    border-right: 1px solid #ced4da;
    text-align: center;
    >i {
        font-size: 25px;
        line-height: 45px;
    }
`;

export const Input = styled(InputStrap)`
    height: 45px;
    border-radius: 10px;
    padding-left: 60px;
`;