import React from "react";
import { Container, Icon, Input } from "./InputWithIcon.style";

export const InputWithLabel = () => {
  return (
    <Container>
      <Icon>
        <i class="fas fa-user-circle"></i>
      </Icon>
      <Input />
    </Container>
  );
};
