import styled from "styled-components";
import {colors} from "../../themes/style";

export const Container = styled.div`
    display: flex;
    cursor: pointer;

    > label {
        height: 20px;
        cursor: pointer;
    }
`;

export const CheckBoxContainer = styled.div`
    width: 20px;
    height: 20px;
    border: 1px solid #ccc;
    margin-right: 10px;
    border-radius: 5px;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: ${({isChecked}) => isChecked ? colors.primary : colors.white};
    .fa-check {
        font-size: 12px;
        color: ${colors.white};
    }
`;