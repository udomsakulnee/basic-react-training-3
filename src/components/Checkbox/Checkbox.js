import React, { useState, useEffect } from "react";
import { CheckBoxContainer, Container } from "./Checkbox.style";
import PropTypes from "prop-types";

export const Checkbox = ({ value, onChange }) => {
  const [isChecked, setIsChecked] = useState(false);

  const handleClick = () => {
    onChange(!isChecked);
    setIsChecked(!isChecked);
  };

  useEffect(() => {
    setIsChecked(value);
  }, [value]);

  return (
    <Container onClick={handleClick}>
      <CheckBoxContainer isChecked={isChecked}>
        {isChecked && <i class="fas fa-check"></i>}
      </CheckBoxContainer>
      <label>label checkbox</label>
    </Container>
  );
};

Checkbox.defaultProps = {
  value: false,
  onChange: () => {},
};

Checkbox.propTypes = {
  value: PropTypes.bool,
  onChange: PropTypes.func,
};
